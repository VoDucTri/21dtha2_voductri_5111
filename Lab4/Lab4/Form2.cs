﻿using Lab4.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab4
{
    public partial class Form2 : Form
    {
        private StudentContextDB context;
        public Form2(StudentContextDB context)
        {
            InitializeComponent();
            this.context = context;
        }
        private void FillFacultyCombobox(List<Faculty> listFalcultys)
        {
            this.cboKhoa.DataSource = listFalcultys;
            this.cboKhoa.DisplayMember = "FacultyName";
            this.cboKhoa.ValueMember = "FacultyID";
        }
        private void BindGrid(List<Faculty> listFalcultys)
        {
            dataGridView1.Rows.Clear();
            foreach (var item in listFalcultys)
            {
                int index = dataGridView1.Rows.Add();
                dataGridView1.Rows[index].Cells[0].Value = item.FacultyID;
                dataGridView1.Rows[index].Cells[1].Value = item.FacultyName;
                dataGridView1.Rows[index].Cells[2].Value = item.TotalProfessor;

            }
        }
        private void Form2_Load(object sender, EventArgs e)
        {
            try
            {
                List<Faculty> listFacuty = context.Faculties.ToList();
                FillFacultyCombobox(listFacuty);
                BindGrid(listFacuty);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            var ask = MessageBox.Show("Ban co muon thoat ?", "Exit", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (ask == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btnThemSua_Click(object sender, EventArgs e)
        {
            try
            {
                int maKhoa = Convert.ToInt32(txtMaKhoa.Text);
                string tenKhoa = cboKhoa.Text.Trim();
                int tongSoGS = Convert.ToInt32(txtTSGS.Text);

                Faculty khoa = context.Faculties.Find(maKhoa);
                if (khoa == null)
                {
                    khoa = new Faculty
                    {
                        FacultyID = maKhoa,
                        FacultyName = tenKhoa,
                        TotalProfessor = tongSoGS
                    };
                    context.Faculties.Add(khoa);
                    context.SaveChanges();
                    MessageBox.Show("Thêm khoa thành công!");
                }
                else
                {
                    khoa.FacultyName = tenKhoa;
                    khoa.TotalProfessor = tongSoGS;
                    context.SaveChanges();
                    MessageBox.Show("Sửa thông tin khoa thành công!");
                }

                List<Faculty> listFaculty = context.Faculties.ToList();
                FillFacultyCombobox(listFaculty);
                BindGrid(listFaculty);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi: " + ex.Message);
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                Faculty dbDelete = context.Faculties.FirstOrDefault(p => p.FacultyID.ToString() == txtMaKhoa.Text.ToString());
                if (dbDelete != null)
                {
                    context.Faculties.Remove(dbDelete);
                    context.SaveChanges();
                    List<Faculty> listFaculty = context.Faculties.ToList();
                    BindGrid(listFaculty);
                }
                else
                {
                    MessageBox.Show("Không tìm thấy khoa!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi: " + ex.Message);
            }
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
